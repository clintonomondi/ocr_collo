# set base image (host OS)
FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt upgrade -y
RUN apt -y install python3.8 
RUN apt -y install build-essential libssl-dev libffi-dev python-dev --fix-missing
RUN apt -y install python3-pip
RUN apt -y install tesseract-ocr
RUN apt-get install -y libsm6 libxext6 libxrender-dev
RUN apt -y install libtesseract-dev

WORKDIR /usr/src/app
EXPOSE 2020
COPY requirements.txt .
RUN pip3 install opencv-python
RUN pip3 install -r requirements.txt
COPY . .
CMD [ "gunicorn",  "wsgi" , "--workers" , "5","--bind", ":2020","--log-level=info" ,"--log-file=logs"]      

