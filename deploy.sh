ssh -o StrictHostKeyChecking=no ubuntu@23.20.226.203 << 'ENDSSH'
  docker pull giktekio/ocr_engine:latest
  docker container stop $(docker container ls -q --filter name=ocr*)
  docker container rm $(docker container ls -a -q --filter name=ocr*)
  docker run -d -p 4700:2020  --name ocr giktekio/ocr_engine:latest 
ENDSSH
