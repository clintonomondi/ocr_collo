from pyimagesearch.transform import four_point_transform
from skimage.filters import threshold_local
import numpy as np
import argparse
import cv2
import imutils
import math

class Binarize(object):
    def __init__(self, image):
        self.image = image
        self.screen_contours = None
        self.grayscale = self.image2bw()

    def image2bw(self):
        gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        return cv2.Canny(gray, 75, 200)
    
    def find_image_contours(self):
        try:
            edged = self.image2bw()
            imagem = (edged)
            cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            outer_cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            cnts = imutils.grab_contours(cnts)
            outer_cnts = imutils.grab_contours(outer_cnts)
            cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:5]
            return (cnts, outer_cnts)
        except:
            return None
    
    def draw_contours(self):
        cnts, outer_cnts = self.find_image_contours()
        least_length = math.inf
        greatest_area = -math.inf
        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)
            i = 0
            if len(approx) == 4 and cv2.contourArea(c) > greatest_area:
                self.screen_contours = approx
        if self.screen_contours is None:
            for c in outer_cnts:
                area = cv2.contourArea(c)
                peri = cv2.arcLength(c, True)
                approx = cv2.approxPolyDP(c, 0.02 * peri, True)
                if len(approx) == 4:
                    self.screen_contours = approx
                    cv2.drawContours(self.grayscale, [self.screen_contours], -1, (0, 255, 0), 2)
        cv2.drawContours(self.grayscale, [self.screen_contours], -1, (0, 255, 0), 2)

    def apply_perspective_transform(self, original_image, ratio):
        self.draw_contours()
        (origH, origW) = original_image.shape[:2]
        self.screen_contours = np.array([[0.0, 0.0], [origW, 0.0], [origW, origH], [0.0, origH]])
        warped = four_point_transform(original_image, self.screen_contours.reshape(4, 2) * 1)
        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
        T = threshold_local(warped, 11, offset = 10, method = "gaussian")
        warped = (warped > T).astype("uint8") * 255
        return warped