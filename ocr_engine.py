from flask import Flask
from flask import jsonify
from flask import request
import base64
import random
import time 
from flask_cors import CORS
from helper_methods.file_helper import convert_and_save
from ocr import Ocr
from front_id import Front_id
import traceback

UPLOAD_FOLDER_OCR = 'ocr_images/'

app = Flask(__name__)
CORS(app)

@app.before_request
def before():
    
    print('SECURITY CHECK HERE ...')

@app.route('/ocr', methods=['GET', 'POST'])
def ocr_engine():
    try:
        app.logger.info('Image sent from frontend')
        image_payload = request.get_json()
    except:
        app.logger.error('Payload sent was empty')
        return jsonify({
            'success': False,
            'reason': 'Image payload is empty'
        })

    time_now = int(time.time())
    random_digit  = random.randint(0,1000)
    # image_payload['document_type'] = 'ID'
    try:
        file_name = UPLOAD_FOLDER_OCR + str(time_now) + '_' + str(random_digit) + '.jpeg'
        app.logger.info('Trying to save image as %s', file_name)
        #write file to disk
       
        convert_and_save(image_payload['nationalid'], file_name)
        app.logger.info('Image saved successfully')
    except Exception as e:
        app.logger.error('Saving failed due to %s', e)
        return jsonify({
            'success': False,
            'reason': 'Error while saving image'
        })

    try:
        print(image_payload['documenttype'])
        app.logger.info('OCR Trying to read fields')
        ocr = Ocr(file_name, image_payload['documenttype'])
        print(ocr.read_fields())
        app.logger.info('Fields read successfully')

        return ocr.read_fields()
    except Exception as e:
        app.logger.info('OCR failed to read image due to %s', e)
        traceback.print_exc()
        return jsonify({
            'success': False,
            'reason': 'Error in OCR'
        })

@app.route('/frontid', methods=['GET', 'POST'])
def read_frontid():
    try:
        app.logger.info('Image sent from frontend')
        image_payload = request.get_json()
        app.logger.info('Image was saved successfuly')
    except:
        app.logger.error('The payload is empty')
        return jsonify({
            'success': False,
            'reason': 'Image payload is empty'
        })

    time_now = int(time.time())
    random_digit  = random.randint(0,1000)
    try:
        file_name = UPLOAD_FOLDER_OCR + str(time_now) + '_' + str(random_digit) + '.jpeg'
        app.logger.info('Trying to save image as %s', file_name)
        #write file to disk
        convert_and_save(image_payload['nationalid'], file_name)
        app.logger.info('Image was saved successfuly')
    except Exception as e:
        app.logger.error('Falied due to %s', e)
        return jsonify({
            'success': False,
            'reason': 'Error while saving image'
        })

    try:
        app.logger.info('Trying to extract data from image')
        front = Front_id(file_name)
        app.logger.info('--------------------------------------------------------')
        app.logger.info('\nExtracted data is %s\n\n', front.data + '\n\n')
        app.logger.info('--------------------------------------------------------')
        return jsonify({
            'success': True,
            'data': front.data
        })
    except Exception as e:
        app.logger.error('Falied due to \n\n%s', e +'\n')
        return jsonify({
            'success': False,
            'reason': 'Error in OCR'
        })

if __name__  == '__main__':
    app.run(host='127.0.0.1', port=2020, debug=True)
