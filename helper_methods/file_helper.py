import base64
import random
import time

UPLOAD_FOLDER_OCR = './ocr_images/'

def convert_and_save(b64_string, file_name):

    image_data = base64.b64decode(b64_string)
    with open(file_name, 'wb') as f:
        f.write(image_data)

""" with open(UPLOAD_FOLDER_OCR + file_name, "wb") as fh:
        fh.write(base64.decodebytes(b64_string.encode())) """