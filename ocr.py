from pyimagesearch.transform import four_point_transform
from skimage.filters import threshold_local
import numpy as np
import argparse
import cv2
import imutils
from scan import Scanner
from binarize import Binarize
from text_recognition import Recognition
import os
import re
from front_id import Front_id
from raw_ocr import Raw_ocr

class Ocr(object):
    def __init__(self, image_path, doc_type_from_frontend):
        self.image_path = image_path
        self.document_type = None
        self.document_number = None
        self.found = False
        self.greyscale_image_path = None
        self.warped_image_path = None
        self.doc_type_from_frontend = doc_type_from_frontend
        scanner = Scanner(self.image_path)
        self.data = None
        image = scanner.read_image()
        if image is not None:
            image = scanner.resize_image()
            binarize = Binarize(image)
            ratio = scanner.get_ratio()
            warped = binarize.apply_perspective_transform(image, ratio)
        
    def read_from_mrz(self):
        print('first attempt')
        if(not self.found):
            print('not found')
            self.recognition = Recognition(self.image_path)
            self.data = self.recognition.mrz_read()

    def remove_shadows(self):
        img = cv2.imread(self.image_path,-1)
        dilated_img = cv2.dilate(img, np.ones((7,7), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(img, bg_img)
        norm_img = diff_img.copy()
        cv2.normalize(diff_img, norm_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        _, thr_img = cv2.threshold(norm_img, 230, 0, cv2.THRESH_TRUNC)
        cv2.normalize(thr_img, thr_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        ocr_text = Raw_ocr(thr_img)
        id_pattern = re.compile(r'[B]{1}(\d){7,9}')
        passport_pattern = re.compile(r'[4|8]?[A-Z]{1,2}[4|8]?(\d){5,7}')
        passport_text = ocr_text.data.split()
        text_from_ocr = re.sub(r'\s+', '',  ocr_text.data)
        print(f'Text from ocr ----> {text_from_ocr}')
        if self.doc_type_from_frontend == 'ID':
            try:
                print('In ID section')
                id_number = re.search(id_pattern, text_from_ocr)
                self.document_number = id_number.group(0)
                self.found = True
                self.document_type = 'ID'
            except:
                print('ID number not matched')
        elif self.doc_type_from_frontend == 'PASSPORT':
            try:
                print('In PASSPORT section')
                print(passport_text)
                for t in passport_text:
                    # passport_number = re.search(passport_pattern, t)
                    try:
                        passport_number = re.search(passport_pattern, t.replace('O', '0').replace('o', '0'))
                        self.document_number = passport_number.group()
                        self.found = True
                        self.document_type = 'PASSPORT'
                        if passport_number is not None:
                            break
                    except:
                        pass
            except:
                print('Passport number not matched')

    def clean_string(self):
        if self.data is not None:
            if self.data['mrz_type'] == 'TD1':
                string = self.data['optional2']
                id_ = string.replace('O', '0').replace('i', '1').replace('Z', '2').replace('S', '5')
                id_ = str(int(id_))
                if len(id_) < 7:
                    padding = 7-len(id_)
                    id_ = padding*'0' + id_
                self.data['optional2'] = id_
            elif self.data['mrz_type'] == 'TD2':
                string = self.data['number']
                self.data['number'] = string.replace('O', '0').replace('o', '0').replace('x', 'K').replace('i', '1').replace('Z', '2').replace('S', '5')
                # print(self.data['number'])
        else:
            self.data = self.data

    def read_fields(self):
        self.remove_shadows()
        if self.document_type is not None:
            print('Document type is not none')
            if self.document_type == 'ID':
                string = self.document_number
                another_string = string
                if string[-1].isalpha():
                    another_string = string[:-1]
                pattern = re.compile(r'(\d){7,9}')
                id_number = str(int(re.search(pattern, another_string).group(0)))
                if len(id_number) < 7:
                    padding = 7-len(id_number)
                    id_number = padding*'0' + id_number
                return dict({'success': True, 'document_type': 'ID', 'id': str(id_number)})
            elif self.document_type == 'PASSPORT':
                stop_at = 1 if self.document_number[1].isalpha() else 0
                self.document_number = self.document_number[:stop_at].replace('4', 'A').replace('8', 'B') + self.document_number[stop_at:]
                print(f'Document number is {self.document_number}')
                pattern = re.compile(r'[4|8]?[A-Za-z]{1,2}[4|8]?(\d){5,7}')
                passport_number = re.match(pattern, self.document_number).group(0)
                return dict({'success': True, 'document_type': 'PASSPORT', 'id': str(passport_number)})
        self.read_from_mrz()
        if self.data is None:
            return dict({'success': False, 'reason': 'Image was not seen'})
        elif self.data['mrz_type'] == 'TD1':
            print(self.data)
            string = self.data['optional2'].replace('<' , '')
            another_string = string
            if string[-1].isalpha():
                another_string = string[:-1]
            self.data['optional2'] = another_string
            pattern = re.compile(r'(\d){7,8}')
            try:
                self.clean_string()
                id_number = self.data['optional2']
                id_number = re.match(pattern, str(id_number)).group(0)
                return dict({'success': True, 'document_type': 'ID', 'id': id_number})
            except:
                return dict({'success': False, 'reason': 'Pattern not matched'})
        elif self.data['mrz_type'] == 'TD3':
            print(self.data)
            passport_pattern = re.compile(r'([A-Za-z]){1,3}(\d){5,8}')
            try:
                passport_number = self.data['number']
                passport_number = re.match(passport_pattern, passport_number).group(0)
                return dict({'success': True, 'document_type': 'PASSPORT', 'id': passport_number})
            except:
                return dict({'success': False})
        else:
            print(self.data)
            return dict({'success': False, 'reason': 'Image not recognized'})