from imutils.object_detection import non_max_suppression
import numpy as np
import pytesseract
import argparse
import cv2
import re
from passporteye import read_mrz

class Front_id(object):
    def __init__(self, image_path, min_confidence = 0.3, padding =0.0):
        image = cv2.imread(image_path)
        self.data = None
        image = cv2.imread(image_path)
        text = pytesseract.image_to_string(image)
        self.data = text
    
    def image_to_string(self):
        return self.data