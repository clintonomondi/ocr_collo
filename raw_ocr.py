from imutils.object_detection import non_max_suppression
import numpy as np
import pytesseract
import argparse
import cv2
import re
from passporteye import read_mrz

class Raw_ocr(object):
    def __init__(self, image, min_confidence = 0.3, padding =0.0):
        self.data = None
        text = pytesseract.image_to_string(image)
        self.data = text
    
    def image_to_string(self):
        return self.data