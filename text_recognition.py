from passporteye import read_mrz

class Recognition(object):
	def __init__(self, image_path):
		self.image_path = image_path

	def mrz_read(self):
		try:
			mrz = read_mrz(self.image_path)
			return mrz.to_dict()
		except:
			return None
