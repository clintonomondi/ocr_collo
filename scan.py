from pyimagesearch.transform import four_point_transform
from skimage.filters import threshold_local
import numpy as np
import argparse
import cv2
import imutils

class Scanner(object):
	def __init__(self, image_path):
		self.image_path = image_path

	def read_image(self):
		try:
			return cv2.imread(self.image_path)
		except:
			return None

	def get_ratio(self):
		try:
			image = self.read_image()
			return image.shape[1] / 500.0
		except:
			return None

	def resize_image(self):
		try:
			image = self.read_image()
			ratio = image.shape[0] / 500.0
			return imutils.resize(image, height = 500)
		except:
			return None